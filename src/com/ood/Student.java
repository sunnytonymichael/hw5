package com.ood;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Student {
    private static Logger log = LogManager.getLogger(Student.class);
    private String name;
    private int id;
    private String phone;
    private String address;
    private int age;
    private String department;

    public Student(int id, String name){
        log.info("Adding a student");
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public int getId() {
        return this.id;
    }

    // package access level
    void setAddress(String address) {
        this.address = address;
    }

    // package access level
    String getAddress() {
        return this.address;
    }

    protected void setPhone(String phone) {
        this.phone = phone;
    }

    protected String getPhone() {
        return this.phone;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return this.age;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDepartment() {
        return this.department;
    }

    public void printStudentInfo() {
        System.out.println("name:" + this.name);
        System.out.println("ID:" + this.id);
        System.out.println("phone:" + this.phone);
        System.out.println("address:" + this.address);
        System.out.println("age: " + this.age);
        System.out.println("department: " + this.department);
    }

    public static void main(String[] args){

        Student alice = new Student(0,"alice");
        alice.setPhone("123");
        alice.setAddress("RPI");
        alice.setAge(22);
        alice.setDepartment("Hill");
        alice.printStudentInfo();

    }
}
