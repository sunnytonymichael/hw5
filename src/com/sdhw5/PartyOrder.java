package com.sdhw5;

import java.util.ArrayList;

public class PartyOrder extends Order{
    private int numPeople;
    private ArrayList<String> decorations;

    public PartyOrder(ArrayList<Meal> M, int numPeople)
    {
        this.meals = new ArrayList<Meal>();
        this.numPeople = numPeople;
    }

    public int getPartySize()
    {
        return this.numPeople;
    }

    public final ArrayList<String> getDecorations() {
        return this.decorations;
    }
}

