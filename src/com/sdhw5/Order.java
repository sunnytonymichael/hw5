package com.sdhw5;

import java.util.ArrayList;
import java.util.List;

public class Order {
    protected List<Meal> meals;

    public Order()
    {
        meals = new ArrayList<Meal>();
    }

    public Order(List<Meal> M){
        this.meals = M;
    }

    public List<Meal> getMeals()
    {
        return meals;
    }

    public void mergeOrder(Order order)
    {
        if(order != null)
        {
            for(Meal meal : order.getMeals())
            {
                meals.add(meal);
            }
        }
    }

    public float getBill()
    {
        int bill = 0;
        for(Meal meal : meals)
        {
            bill += meal.getPrice();
        }
        return bill;
    }

}
