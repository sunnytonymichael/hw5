package com.sdhw5;

class NoTableException extends Exception{

    public NoTableException(PartyOrder p)
    {
        super("No table available for party size: " + p.getPartySize());
    }
}
