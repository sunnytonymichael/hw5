package com.test;

import com.sdhw5.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {
    private List<Meal> meals1 = new ArrayList<Meal>();
    private List<Meal> meals2 = new ArrayList<Meal>();


    @Test
    public void getMealsTest(){
        Meal beef = new Meal(1);
        Meal fish = new Meal(2);
        meals1.add(beef);
        meals1.add(fish);
        Order O = new Order(meals1);
        List<Meal> test = O.getMeals();
        assertEquals(1, test.get(0).getPrice());
        assertEquals(2, test.get(1).getPrice());
    }

    @Test
    public void mergeOrderTest(){
        Meal beef = new Meal(1);
        meals1.add(beef);
        Order O1 = new Order(meals1);
        Meal meatball = new Meal(3);
        meals2.add(meatball);
        Order O2 = new Order(meals2);
        O1.mergeOrder(O2);
        List<Meal> test = O1.getMeals();
        assertEquals(1,test.get(0).getPrice());
        assertEquals(3,test.get(1).getPrice());
    }

    @Test
    public void getBillTest(){
        Meal beef = new Meal(1);
        Meal meatball = new Meal(2);
        meals1.add(beef);
        meals1.add(meatball);
        Order O = new Order(meals1);
        assertEquals(3,O.getBill());
    }

}