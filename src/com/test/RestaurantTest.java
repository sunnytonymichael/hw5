package com.test;
import com.sdhw5.*;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RestaurantTest {


    @Test
    void addDescriptionTest() {
        Restaurant restaurant = new Restaurant();
        Table table1 = new Table(5);
        restaurant.addTable(table1);
        assertEquals("Table: 0, table size: 5, isAvailable: true. No current order for this table.\n" +
                "*****************************************\n", restaurant.restaurantDescription());
    }
}