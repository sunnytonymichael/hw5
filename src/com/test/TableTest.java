package com.test;
import com.sdhw5.*;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TableTest {

    @Test
    void getCapacityTest() {
        Table myTable = new Table(5);
        assertEquals(5, myTable.getCapacity());
    }

    @Test
    void isAvailableTest() {
        Table myTable = new Table(5);
        assertEquals(true, myTable.isAvailable());
    }

    @Test
    void markAvailableTest() {
        Table myTable = new Table(5);
        myTable.markUnavailable();
        myTable.markAvailable();
        assertEquals(true, myTable.isAvailable());
    }

    @Test
    void markUnavailableTest() {
        Table myTable = new Table(5);
        myTable.markUnavailable();
        assertEquals(false, myTable.isAvailable());
    }

    @Test
    void getCurrentOrder() {
        List<Meal> meal = new ArrayList<Meal>();
        Meal beef = new Meal(1);
        Meal fish = new Meal(2);
        meal.add(beef);
        meal.add(fish);
        Order O = new Order(meal);
        Table myTable = new Table(5);
        myTable.setOrder(O);
        Order test = myTable.getCurrentOrder();
        List<Meal> testmeal = test.getMeals();
        assertEquals(1, testmeal.get(0).getPrice());
        assertEquals(2, testmeal.get(1).getPrice());
    }

    @Test
    void compareToTest() {
        Table tb1 = new Table(2);
        Table tb2 = new Table(5);
        assertEquals(3,tb2.compareTo(tb1));
    }
}