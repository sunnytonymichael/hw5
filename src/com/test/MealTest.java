package com.test;

import com.sdhw5.*;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class MealTest {
    @Test
    public void mealGetPriceTest(){
        Meal myMeal = new Meal(6);
        assertEquals(6, myMeal.getPrice());
    }
}