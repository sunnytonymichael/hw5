package com.test;
import com.sdhw5.*;

import org.junit.jupiter.api.Test;


import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class PartyOrderTest {
    @Test
    public void getMealsTest(){
        PartyOrder myParty = new PartyOrder(new ArrayList<Meal>(), 5);
        assertEquals(5, myParty.getPartySize());
    }
}